// import "./App.css";
// import React, { useEffect, useRef, useState } from "react"
// import { CopyToClipboard } from "react-copy-to-clipboard"
// import Peer from "simple-peer"
// import io from "socket.io-client"
// import "./App.css"


// const socket = io.connect('http://localhost:5000')
// function App() {
//   	const [ me, setMe ] = useState("")
// 	const [ stream, setStream ] = useState()
// 	const [ receivingCall, setReceivingCall ] = useState(false)
// 	const [ caller, setCaller ] = useState("")
// 	const [ callerSignal, setCallerSignal ] = useState()
// 	const [ callAccepted, setCallAccepted ] = useState(false)
// 	const [ idToCall, setIdToCall ] = useState("")
// 	const [ callEnded, setCallEnded] = useState(false)
// 	const [ name, setName ] = useState("Madhan")
// 	const [ users, setUsers ] = useState([])
// 	const myVideo = useRef()
// 	const userVideo = useRef()
// 	const connectionRef= useRef()

// 	useEffect(() => {
// 		navigator.mediaDevices.getUserMedia({ video: true, audio: true }).then((stream) => {
// 			setStream(stream)
// 			myVideo.current.srcObject = stream
// 		})

// 		socket.on("me", (id) => {
// 			setMe(id)

// 				socket.emit("joinRoom", {roomId : "roomId",  data :{
// 					id: id,
// 				}} )
// 		})

// 		socket.on("userJoined", ({id}) =>{
// 				callUser(id)
// 		})

// 		socket.on("callUser", (data) => {
// 				setReceivingCall(true)
// 				setCaller(data.from)
// 				setName(data.name)
// 				setCallerSignal(data.signal)
// 		})
// 	}, [])

// 	const callUser = (id) => {
// 		const peer = new Peer({
// 			initiator: true,
// 			trickle: false,
// 			stream: stream
// 		})
// 		peer.on("signal", (data) => {
// 			socket.emit("callUser", {
// 				userToCall: id,
// 				signalData: data,
// 				from: me,
// 				name: name
// 			})
// 		})
// 		peer.on("stream", (stream) => {
// 				userVideo.current.srcObject = stream
// 		})
// 		socket.on("callAccepted", (signal) => {
// 			setCallAccepted(true)
// 			peer.signal(signal)
// 		})

// 		connectionRef.current = peer
// 	}


// const leaveCall = () => {
// 	setCallEnded(true)
// 	connectionRef.current.destroy()
// }

// const answerCall =() =>  {
// 	setCallAccepted(true)
// 	const peer = new Peer({
// 		initiator: false,
// 		trickle: false,
// 		stream: stream
// 	})
// 	peer.on("signal", (data) => {
// 		socket.emit("answerCall", { signal: data, to: caller })
// 	})
// 	peer.on("stream", (stream) => {
// 		userVideo.current.srcObject = stream
// 	})

// 	peer.signal(callerSignal)
// 	connectionRef.current = peer
// }
//   return (
//     <div className="App">
//         <div style={{width :"100%",display:"flex", flexDirection:"row",height:"10vh",  alignItems:"center",justifyContent:"space-between"}}>
//           <div style={{display:"flex", flexDirection:"row",height:"10vh",  alignItems:"center",justifyContent:"space-between"}}>
//             <span style={{padding:10, fontSize:30, fontWeight:"bold"}}>Monkly</span>
//             <div style={{padding:5 , marginLeft:20, backgroundColor:"#062925", borderRadius:2 }}>/daily-standup 4/50 </div>
//           </div>
//           <div style={{display:"flex", flexDirection:"row",height:"10vh",  alignItems:"center",justifyContent:"space-between"}}>
//              <div  style={{width:35, height:35, marginInline:20, backgroundColor:"#062925", borderRadius:8,justifySelf:"flex-end", display:'flex', justifyContent:"center", alignItems:"center" , flexDirection:"column"}}>* </div>
//              <div  style={{width:35, height:35, marginInline:20, backgroundColor:"#f8dbd5", borderRadius:8,justifySelf:"flex-end", display:'flex', justifyContent:"center", alignItems:"center" , flexDirection:"column"}}>* </div>
//           </div>
//         </div>
//         <div className="container">
//           {/* <div style={{display:"flex", flexDirection:"row",  justifyContent:"space-around"}}> */}
//             <div className="grid-item">
//               <span className="name-card" style={{ paddingInline: 20,paddingBlock: 3}}>{name}</span>
//               {/* <img src={require("./assets/1.jpg").default} style={{borderRadius:9}} height={"100%"} width={"100%"}  /> */}
//               {stream &&  <video playsInline  muted ref={myVideo} autoPlay style={{borderRadius:9}}   height={"100%"} width={"100%"} />}
//             </div>
//             <div  className="grid-item">
//               <span style={{ margin:10, position:"absolute",  backgroundColor : "#062925",  paddingInline:20, paddingBlock:3, display:"flex", alignItems:"center", justifyContent:"center", borderRadius:5}}>Akari</span>
//               {/* <img src={require("./assets/3.jpg").default} style={{borderRadius:9}} height={"100%"} width={"100%"}  /> */}
// 					<video playsInline ref={userVideo} autoPlay  style={{borderRadius:9}}   height={"100%"} width={"100%"}/>
//             </div>

//         </div>
//         <div className="options"  style={{display:"flex",height:"10vh",  alignItems:"center",justifyContent:"center"}}>
//         <div style={{display:"flex", flexDirection:"row", alignItems:"center",justifyContent:"space-between"}}>
// 			{receivingCall && !callAccepted ? (
// 				<div className="caller">
// 					<div   onClick={answerCall} style={{display:"flex", flexDirection:"column", justifyContent:"space-between", alignItems:"center"}}>   <div style={{marginInline:20, padding:5, backgroundColor:"#062925", borderRadius:8,justifySelf:"flex-end", display:'flex', justifyContent:"center", alignItems:"center" , flexDirection:"column"}}> Answer </div> <span style={{fontSize:14, marginTop:3}}></span> </div>
// 				</div>
// 			) : null}
//             <div style={{display:"flex", flexDirection:"column", justifyContent:"space-between", alignItems:"center"}}>   <div style={{padding:5, marginInline:20, backgroundColor:"#062925", borderRadius:8,justifySelf:"flex-end", display:'flex', justifyContent:"center", alignItems:"center" , flexDirection:"column"}}> Leave </div> <span style={{fontSize:14, marginTop:3}}></span> </div>
//         	</div>
//         </div>
//      </div>
//   );
// }

// export default App;
